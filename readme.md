# Assignement 3: E-commerce

## Studenti
- Samuele Pipitone 830595
- Marco Campione 844555
## Gitlab
https://gitlab.com/samuele.pipitone/2022_assignment3_e-commerce
## Descrizione dell'applicazione
L'applicazione E-commerce permette di iscriversi ad un e-commerce in cui è possibile comprare e vendere oggetti di tre tipi: libri, musica e film. C'è la possibilità di creare più di un carrello per utente e di aggiungere oggetti ad essi. E' inoltre possibile vendere oggetti inserendo alcune informazioni e saranno visibili agli altri utenti tramite due funzioni: ricerca di tutti gli oggetti o ricerca per nome esatto. Inoltre è possibile "seguire" un altro utente e comprando un oggetto messo da lui in vendita avrai un 15% di sconto.
## Utilizzo
E' possibile utilizzare l'applicazione tramite linea di comando, al lancio apparirà un menù testuale che illustrerà le varie operazioni effettuabili, la scelta verrà effettuata tramite numero. Ci sono due modi di utilizzare l'applicazione: 
- manuale: inserendo le informazioni richieste dal menu 
- automatica: selezionando l'opzione 3 ("Perform an automatic test of the operations"), il quale fa delle operazioni in automatico per mostrare il funzionamento di ogni feature in automatico.
## Ruolo delle classi
### Model:
Nella parte model abbiamo inserito tutte le classi entità, utilizzate per fare l'ORM. Ogni oggetto avrà i corrispettivi campi del database e le relazioni, i metodi getter/setter e i costruttori.
### Repository:
Le classi repository estendono un CrudRepository (ereditano quindi i suoi metodi), una classe di Spring necessaria per effettuare le operazioni CRUD di base. Per svolgere delle operazioni personalizzate abbiamo esteso poi i singoli repository e implementato i metodi che ci servivano.
### Ecommerce:
Contiene una classe di configurazione per far funzionare la parte JPA (con tutti i dati necessari). Inoltre contiene la classe main che permette di lanciare l'applicazione.
### Utils:
Contiene due classi con metodi necessari per il funzionamento dell'applicazione, in particolare abbiamo utilizzato una classe per la gestione del login/logout e una classe per svolgere tutte le operazioni mostrate dal menù
### Console:
Contiene varie classi necessarie sia per mostrare i vari menù testuali in output che per prendere le informazioni in input dall'utente e verificare che siano corrette.

 ## Diagramma entità relazione
![IMAGE_DESCRIPTION](image/diagramma%20ecommerce.png)

## Descrizione delle operazioni CRUD
Le operazioni CRUD di base le abbiamo ottenute grazie al CrudRepository di Spring, esso fornisce: save, find e delete, più qualche altro metodo utile. Le operazioni personalizzate le abbiamo scritte estendendo il CrudRepository in delle nostre interfacce, in particolare abbiamo esteso CarrelloRepository, OggettoRepository e UtenteRepository. Alcune operazioni che abbiamo implementato sono:
- customFindFollowers(Long id): questo metodo ritorna tutti i follower di un utente in input, passato per id. In particolare l'oggetto Utente ha una relazione con se stesso di tipo many to many, è stato quindi necessario accedere alla tabella di join e mappare manualmente l'oggetto ottenuto in quanto Hibernate non è in grado di autoconvertirla.
- findOggettiByCarrelloId(Long id): questo metodo ritorna una lista di Oggetti presi da un Carrello (ottenuto per id), anche questa relazione è del tipo many to many, abbiamo quindi dovuto accedere alla join table e fare il map manuale. 