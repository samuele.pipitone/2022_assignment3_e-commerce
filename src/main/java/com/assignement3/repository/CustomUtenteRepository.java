package com.assignement3.repository;

import java.util.List;

import com.assignement3.model.Utente;

public interface CustomUtenteRepository {
	Utente customFindMethod(Long Id);
	Utente customFindByUsernamePassword(String username, String password);
	Utente customFindByUsername(String username);
	List<Utente> customFindOthers(String username);
	List<Utente> customFindFollowers(Long id);
	List<Utente> getAllUsers();
}
