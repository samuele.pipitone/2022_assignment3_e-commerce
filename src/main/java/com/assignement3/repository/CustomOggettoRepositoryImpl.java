package com.assignement3.repository;

import java.util.List;

import com.assignement3.model.Oggetto;
import com.assignement3.utils.Login;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;

public class CustomOggettoRepositoryImpl implements CustomOggettoRepository {

	@PersistenceContext
	private EntityManager entityManager;

	public Oggetto findByName(String name) throws Exception {

		String query = "from Oggetto o where o.title = :name";
		Oggetto oggetto = (Oggetto) entityManager.createQuery(query).setParameter("name", name).getSingleResult();
		return oggetto;
	}

	public void show() {
		String query = "from Oggetto";
		@SuppressWarnings("unchecked")
		List<Oggetto> oggetti = (List<Oggetto>) entityManager.createQuery(query).getResultList();
		
		for (Oggetto o : oggetti) {
			if(!Login.currentUser.equals(o.getSeller())) {
				System.out.println(o.toString());
			}
		}
	}

	@Override
	@Transactional
	public Oggetto buyItem(String name) {
		Oggetto o;
		try {
			o = findByName(name);
			entityManager.remove(o);
			return o;
		} catch (Exception e) {
			System.out.println("No item found with that name!");
			return null;
		}
	}
	
	public void showAllItems() {
		String query = "from Oggetto";
		@SuppressWarnings("unchecked")
		List<Oggetto> oggetti = (List<Oggetto>) entityManager.createQuery(query).getResultList();
		
		for (Oggetto o : oggetti) {
			System.out.println(o.toString());
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<Oggetto> getAllItems() {
		String query = "from Oggetto";
		return (List<Oggetto>) entityManager.createQuery(query).getResultList();
	}

}
