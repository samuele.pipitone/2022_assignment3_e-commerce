package com.assignement3.repository;

import org.springframework.data.repository.CrudRepository;

import com.assignement3.model.Musica;

public interface MusicaRepository extends CrudRepository<Musica, Long> {

}