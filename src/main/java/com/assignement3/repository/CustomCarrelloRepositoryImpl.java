package com.assignement3.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.assignement3.model.Carrello;
import com.assignement3.model.Oggetto;
import com.assignement3.model.Utente;

import jakarta.annotation.PostConstruct;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.Query;

public class CustomCarrelloRepositoryImpl implements CustomCarrelloRepository {

	@PersistenceContext
    private EntityManager entityManager;
	
	/***
	* Questo metodo trova il carrello con l'id passato e ritorna tutti i suoi oggetti.
	* Bisogna utilizzare necessariamente una query nativa in SQL per poter
	* accedere alla tabella di join oggetti_carrelli
	* 
	* @param id
	* */
	@SuppressWarnings("unchecked")
	@Override
	public List<Oggetto> findOggettiByCarrelloId(Long id) {
	
		Query q = entityManager.createNativeQuery(
				"select o.OGGETTO_ID, o.title, o.price, o.description "
				+ "from oggetti_carrelli as oc, oggetto as o "
				+ "where oc.CARRELLO_ID = ? "
				+ "and o.OGGETTO_ID = oc.OGGETTO_ID");
		q.setParameter(1, id);
		List<Oggetto> oggetti = map(q.getResultList());
		
		return oggetti;
	}
	
    @PostConstruct
    public void postConstruct() {
        Objects.requireNonNull(entityManager);
    }
    
    /**
     * Metodo utilizzato per mappare manualmente da una generica lista di oggetti
     * alla nostra classe Oggetto. Richiede che i campi dell'oggetto generico siano 
     * in ordine corretto e del tipo: Long, String, double, String.
     * 
     * @param objects
     * @return List<Oggetto> oggetti
     */
    public List<Oggetto> map(List<Object> objects) {
    	
    	List<Oggetto> oggetti = new ArrayList<Oggetto>();
    	
    	try {
    		for(Object obj : objects) {
    			Oggetto o = new Oggetto();
    			o.setId((Long)((Object[]) obj)[0]);
    			o.setTitle((String)((Object[]) obj)[1]);
    			o.setPrice((double)((Object[]) obj)[2]);
    			o.setDescription((String)((Object[]) obj)[3]);
    			oggetti.add(o);
    		}
    	} catch(Exception e) {
    		System.out.println("Generic error to map");
    	}
    	
    	return oggetti;
    }

	@SuppressWarnings("unchecked")
	@Override
	public List<Carrello> findCarrelli() {
		String query = "from Carrello";
		return (List<Carrello>) entityManager.createQuery(query).getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Carrello> findCarrelliByUser(Utente user) {
		String query = "from Carrello where user = :user_id ";
		return (List<Carrello>) entityManager.createQuery(query)
				.setParameter("user_id", user)
				.getResultList();
	}
	
	
}
