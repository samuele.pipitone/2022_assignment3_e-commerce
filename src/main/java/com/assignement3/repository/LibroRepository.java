package com.assignement3.repository;

import org.springframework.data.repository.CrudRepository;

import com.assignement3.model.Libro;

public interface LibroRepository extends CrudRepository<Libro, Long>{

}
