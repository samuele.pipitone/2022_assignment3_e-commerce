package com.assignement3.repository;

import org.springframework.context.annotation.Primary;
import org.springframework.data.repository.CrudRepository;

import com.assignement3.model.Oggetto;

@Primary
public interface OggettoRepository extends CrudRepository<Oggetto, Long>, CustomOggettoRepository {

}
