package com.assignement3.repository;

import org.springframework.data.repository.CrudRepository;

import com.assignement3.model.Carrello;

public interface CarrelloRepository extends CrudRepository<Carrello, Long>, CustomCarrelloRepository {

}
