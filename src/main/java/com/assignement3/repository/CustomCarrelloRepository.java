package com.assignement3.repository;

import java.util.List;

import com.assignement3.model.Carrello;
import com.assignement3.model.Oggetto;
import com.assignement3.model.Utente;

public interface CustomCarrelloRepository {
	List<Oggetto> findOggettiByCarrelloId(Long id);
	List<Carrello> findCarrelli();
	List<Carrello> findCarrelliByUser(Utente user);
}
