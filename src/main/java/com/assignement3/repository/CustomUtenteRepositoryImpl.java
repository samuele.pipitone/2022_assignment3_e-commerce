package com.assignement3.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.assignement3.model.Utente;

import jakarta.annotation.PostConstruct;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.Query;

@SuppressWarnings("unchecked")
public class CustomUtenteRepositoryImpl implements CustomUtenteRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Utente customFindMethod(Long id) {
        return (Utente) entityManager.createQuery("FROM Utente u WHERE u.id = :id")
                .setParameter("id", id)
                .getSingleResult();
    }

    @PostConstruct
    public void postConstruct() {
        Objects.requireNonNull(entityManager);
    }
    
    public Utente customFindByUsernamePassword(String username, String password) {
    	try {
	    	String query = "FROM Utente u WHERE u.username = :username AND u.password = :password";
	    	return (Utente) entityManager.createQuery(query)
	    			.setParameter("username", username)
	    			.setParameter("password", password)
	    			.getSingleResult();
    	} catch(Exception e) {
    		return null;
    	}
    }
    
    public Utente customFindByUsername(String username) {
    	try {
	    	String query = "FROM Utente u WHERE u.username = :username";
	    	return (Utente) entityManager.createQuery(query)
	    			.setParameter("username", username)
	    			.getSingleResult();
    	} catch(Exception e) {
    		return null;
    	}
    }
    
	public List<Utente> customFindOthers(String username) {
    	try {
	    	String query = "FROM Utente u WHERE u.username <> :username";
	    	return  (List<Utente>) entityManager.createQuery(query)
	    			.setParameter("username", username)
	    			.getResultList();
    	} catch(Exception e) {
    		return null;
    	}
    }
    
    //TODO deve ritornare tutti gli utenti che seguono quello passato in input
	public List<Utente> customFindFollowers(Long id) {
		Query query = entityManager.createNativeQuery(
				"select u.UTENTE_ID, u.username, u.password "
    			+ "FROM utente_utente as uu, utente as u "
    			+ "where uu.FOLLOWER_ID = ? "
    			+ "and u.UTENTE_ID = uu.FOLLOWING_ID "); //creare la query
    	query.setParameter(1, id);
    	List<Utente> utenti = map(query.getResultList());
		return utenti;
    }
    
    /**
     * Metodo utilizzato per mappare manualmente da una generica lista di oggetti
     * alla nostra classe Utenti. Richiede che i campi dell'oggetto generico siano 
     * in ordine corretto e del tipo: Long, String, String.
     * 
     * @param objects
     * @return List<Utente> utenti
     */
    public List<Utente> map(List<Object> objects) {
    	
    	List<Utente> utenti = new ArrayList<Utente>();
    	
    	try {
    		for(Object obj : objects) {
    			Utente u = new Utente();
    			u.setId((Long)((Object[]) obj)[0]);
    			u.setUsername((String)((Object[]) obj)[1]);
    			u.setPassword((String)((Object[]) obj)[2]);
    			utenti.add(u);
    		}
    	} catch(Exception e) {
    		System.out.println("Generic error to map");
    	}
    	
    	return utenti;
    }

	@Override
	public List<Utente> getAllUsers() {
		try {
	    	String query = "FROM Utente u";
	    	return  (List<Utente>) entityManager.createQuery(query).getResultList();
    	} catch(Exception e) {
    		return null;
    	}
	}
}
