package com.assignement3.repository;

import org.springframework.data.repository.CrudRepository;

import com.assignement3.model.Film;

public interface FilmRepository extends CrudRepository<Film, Long> {

}
