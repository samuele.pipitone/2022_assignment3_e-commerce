package com.assignement3.repository;

import java.util.List;

import com.assignement3.model.Oggetto;
import com.assignement3.utils.Login;

public interface CustomOggettoRepository {
	Oggetto findByName(String name) throws Exception;
	void show();
	void showAllItems();
	Oggetto buyItem(String name);
	List<Oggetto> getAllItems();
}
