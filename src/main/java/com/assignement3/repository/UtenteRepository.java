package com.assignement3.repository;

import java.util.List;

import org.springframework.context.annotation.Primary;
import org.springframework.data.repository.CrudRepository;

import com.assignement3.model.Utente;

@Primary
public interface UtenteRepository extends CrudRepository<Utente, Long>, CustomUtenteRepository {
	List<Utente> findByUsername(String username);
}
