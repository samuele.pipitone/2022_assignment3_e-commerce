package com.assignement3.repository;

import org.springframework.data.repository.CrudRepository;

import com.assignement3.model.Ecommerce;

public interface EcommerceRepository extends CrudRepository<Ecommerce, Long> {
	
}
