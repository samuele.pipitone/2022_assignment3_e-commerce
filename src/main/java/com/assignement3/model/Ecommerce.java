package com.assignement3.model;

import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name = "ecommerce")
public class Ecommerce {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="ECOMMERCE_ID")
	private Long ecommerce_id;
	
	@OneToMany(cascade=CascadeType.ALL, mappedBy="ecommerce", fetch = FetchType.EAGER)
	private List<Utente> users;
	
	@OneToMany(cascade=CascadeType.ALL, mappedBy="ecommerce", fetch = FetchType.EAGER)
	private List<Oggetto> objects;
	
	public Long getId() {
		return ecommerce_id;
	}
	
	public List<Utente> getUsers() {
		return users;
	}
	
	public void setUsers(List<Utente> users) {
		this.users = users;
	}
	
	public List<Oggetto> getObjects() {
		return objects;
	}
	
	public void setObjects(List<Oggetto> objects) {
		this.objects = objects;
	}

	@Override
	public String toString() {
		return "Ecommerce [ecommerce_id=" + ecommerce_id + ", users=" + users + ", objects=" + objects + "]";
	}

}
