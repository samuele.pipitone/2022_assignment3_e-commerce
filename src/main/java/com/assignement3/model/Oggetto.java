package com.assignement3.model;

import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Inheritance;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.InheritanceType;

@Entity
@Table(name = "oggetto")
@Inheritance(strategy=InheritanceType.JOINED)
public class Oggetto {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="OGGETTO_ID")
	private Long id;
	private String title;
	private double price;
	private String description;
	
	@ManyToMany(mappedBy="objects", fetch = FetchType.EAGER)
	private List<Carrello> carts;
	
	@ManyToOne
	@JoinColumn(name="ECOMMERCE_ID")
	private Ecommerce ecommerce;
	
	@ManyToOne
	private Utente seller;
	
	public Oggetto() {}
	
	public Oggetto(String title) {
		this.title = title;
	}
	
	public Oggetto(String title, double price, String description) {
		this.title = title;
		this.price = price;
		this.description = description;
	}
	
	public Oggetto(String title, double price, String description, Utente seller) {
		this.title = title;
		this.price = price;
		this.description = description;
		this.seller = seller;
	}
	
	public Oggetto(long id, String title, double price, String description) {
		this.id = id;
		this.title = title;
		this.price = price;
		this.description = description;
	}
	
	public Utente getSeller() {
		return seller;
	}

	public void setSeller(Utente seller) {
		this.seller = seller;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Ecommerce getEcommerce() {
		return ecommerce;
	}
	public void setEcommerce(Ecommerce ecommerce) {
		this.ecommerce = ecommerce;
	}
	public List<Carrello> getCarts() {
		return carts;
	}
	public void setCarts(List<Carrello> carts) {
		this.carts = carts;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Long getId() {
		return id;
	}

	@Override
	public String toString() {
		return "Oggetto [title= " + title + ", price=" + price + ", description=" + description
				+ ", seller=" + seller + "]";
	}
	
}
