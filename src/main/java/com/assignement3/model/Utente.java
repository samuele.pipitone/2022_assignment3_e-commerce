package com.assignement3.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;

@Entity
public class Utente {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="UTENTE_ID")
	private Long id;
	private String username;
	private String password;
	
	@ManyToOne
	@JoinColumn(name="ECOMMERCE_ID")
	private Ecommerce ecommerce;
	
	@OneToMany(cascade=CascadeType.ALL, mappedBy="user", fetch = FetchType.EAGER)
	private List<Carrello> carts;
	
	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinTable(name="utente_utente",
	                joinColumns={@JoinColumn(name="FOLLOWER_ID")}, 
	                inverseJoinColumns={@JoinColumn(name="FOLLOWING_ID")})
	private List<Utente> followers = new ArrayList<Utente>();
	
	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinTable(name="utente_utente", 
	                joinColumns={@JoinColumn(name="FOLLOWING_ID")}, 
	                inverseJoinColumns={@JoinColumn(name="FOLLOWER_ID")})
	private List<Utente> following = new ArrayList<Utente>();
	
	@OneToMany(cascade=CascadeType.ALL, mappedBy="seller")
	private List<Oggetto> sellingItems = new ArrayList<Oggetto>();
	
	public Utente() {};
	
	public Utente(String username, String password) {
		this.username = username;
		this.password = password;
	}
	
	public Utente(String username, String password, List<Utente> followers) {
		this.username = username;
		this.password = password;
		this.followers = followers;
	}
	
	public Ecommerce getEcommerce() {
		return ecommerce;
	}

	public void setEcommerce(Ecommerce ecommerce) {
		this.ecommerce = ecommerce;
	}

	public Long getId() {
        return id;
	}
    
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public void addFollowing(Utente utente) throws Exception {
		if(following.contains(utente)) {
			throw new Exception();
		}
		else {
			following.add(utente);
		}
	}
	
	@Override
	public String toString() {
		return "User [id=" + id + ", username=" + username + ", password=" + password + "]";
	}
	
	@Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Utente person = (Utente) o;
        return Objects.equals(id, person.id) && Objects.equals(username, person.username) && Objects.equals(password, person.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, username, password);
    }

	public void setId(Long id) {
		this.id = id;
	}

	public List<Utente> getFollowers() {
		return followers;
	}

	public void setFollowers(List<Utente> followers) {
		this.followers = followers;
	}

	public List<Utente> getFollowing() {
		return following;
	}

	public void setFollowing(List<Utente> following) {
		this.following = following;
	}
	
	
}
