package com.assignement3.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;

@Entity
public class Libro extends Oggetto {
	
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private int pages;
	private String author;
	
	public Libro() {}
	
	public Libro(String title, double price, String description, int pages, String author) {
		super(title, price, description);
		this.pages = pages;
		this.author = author;
	}
	
	public Libro(String title, double price, String description, int pages, String author, Utente seller) {
		super(title, price, description, seller);
		this.pages = pages;
		this.author = author;
	}
	
	public int getPages() {
		return pages;
	}
	public void setPages(int pages) {
		this.pages = pages;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public Long getId() {
		return id;
	}
}
