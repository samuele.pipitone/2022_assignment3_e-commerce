package com.assignement3.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;

@Entity
public class Film extends Oggetto {

	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private double duration;
	
	public Film() {}
	
	public Film(String title, double price, String description, double duration) {
		super(title, price, description);
		this.duration = duration;
	}
	
	public Film(String title, double price, String description, double duration, Utente seller) {
		super(title, price, description, seller);
		this.duration = duration;
	}
	
	public double getDuration() {
		return duration;
	}
	public void setDuration(double duration) {
		this.duration = duration;
	}
	public Long getId() {
		return id;
	}
	
}
