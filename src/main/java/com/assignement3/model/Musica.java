package com.assignement3.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;

@Entity
public class Musica extends Oggetto {
	
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private double length;
	private String author;
	
	public Musica() {}
	
	public Musica(String title, double price, String description, double length, String author) {
		super(title, price, description);
		this.author = author;
		this.length = length;
	}
	
	public Musica(String title, double price, String description, double length, String author, Utente seller) {
		super(title, price, description, seller);
		this.author = author;
		this.length = length;
	}
	
	public double getLength() {
		return length;
	}
	public void setLength(double length) {
		this.length = length;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public Long getId() {
		return id;
	}
	
}
