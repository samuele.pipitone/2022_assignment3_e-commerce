package com.assignement3.model;

import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "carrello")
public class Carrello {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="CARRELLO_ID")
	private Long id;
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name="oggetti_carrelli",
			joinColumns = { @JoinColumn(name = "CARRELLO_ID") }, 
            inverseJoinColumns = { @JoinColumn(name = "OGGETTO_ID") })
	private List<Oggetto> objects;
	
	@ManyToOne
	@JoinColumn(name="ECOMMERCE_ID")
	private Ecommerce ecommerce;
	
	@ManyToOne
	@JoinColumn(name="UTENTE_ID")
	Utente user;
	
	private String name;
	
	public Carrello() {};
	
	public Carrello(String name) {
		this.name = name;
	}
	
	public Long getId() {
		return id;
	}

	public List<Oggetto> getObjects() {
		return objects;
	}
	
	public void setObjects(List<Oggetto> objects) {
		this.objects = objects;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Utente getUser() {
		return user;
	}

	public void setUser(Utente user) {
		this.user = user;
	}
	
	public void addObject(Oggetto o) {
		objects.add(o);
	}

	@Override
	public String toString() {
		return "Carrello [id=" + id + ", name=" + name + "]";
	}
	
}
