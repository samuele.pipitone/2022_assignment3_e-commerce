package com.assignement3.Ecommerce;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import com.assignement3.console.Menu;
import com.assignement3.console.MenuItem;
import com.assignement3.model.Utente;
import com.assignement3.repository.CarrelloRepository;
import com.assignement3.repository.OggettoRepository;
import com.assignement3.repository.UtenteRepository;
import com.assignement3.utils.Login;
import com.assignement3.utils.Operations;

@Import(PersistenceJPAConfig.class)
@SpringBootApplication
public class EcommerceApplication implements CommandLineRunner {

	private static Logger LOG = LoggerFactory.getLogger(EcommerceApplication.class);
	@Autowired
	private OggettoRepository oggettoRepository;
	@Autowired
	private CarrelloRepository carrelloRepository;
	@Autowired
	protected DataSource dataSource;
	@Autowired
	private UtenteRepository utenteRepository;
	
	Login login;
	Operations operations;
	
	//MAIN
	public static void main(String[] args) {
		LOG.info("STARTING THE APPLICATION");
		SpringApplication.run(EcommerceApplication.class, args);
		LOG.info("APPLICATION FINISHED");
	}
	
	@Override
	public void run(String... args) {
		login = new Login(utenteRepository);
		operations = new Operations(oggettoRepository, carrelloRepository, utenteRepository, dataSource);
		Utente admin = login.createRuntimeUser();
		operations.initDatabase(admin);
		
		System.out.println("Welcome to our Ecommerce!");
		homeMenu();
	}
	
	private void homeMenu() {
		Menu menu = new Menu();
		menu.setTitle("*** Home Menu ***");
		menu.addItem(new MenuItem("Login", this, "performLogin"));
		menu.addItem(new MenuItem("Register", this, "performRegister"));
		menu.addItem(new MenuItem("Perform an automatic test of the operations", this, "showcase"));
		menu.execute();
	}
	
	public void performLogin() {
		login.authenticate();
		mainMenu();
	}
	
	public void performRegister() {
		login.register();
		mainMenu();
	}
	
	public void mainMenu() {
		Menu menu = new Menu();
		menu.setTitle("*** Main Menu ***");
		menu.addItem(new MenuItem("Sell an item", operations, "sell"));
		menu.addItem(new MenuItem("Buy an item", operations, "buy"));
		menu.addItem(new MenuItem("Search for an item by its name", operations, "searchItem"));
		menu.addItem(new MenuItem("Show all items in the ecommerce", operations, "showAllItems"));
		menu.addItem(new MenuItem("Look at my carts", operations, "showCart"));
		menu.addItem(new MenuItem("Add item to a cart", operations, "addItemToCart"));
		menu.addItem(new MenuItem("Create a new cart", operations, "addNewCart"));
		menu.addItem(new MenuItem("Follow a user", operations, "followUser"));
		menu.addItem(new MenuItem("Show my followers", operations, "showFollowers"));
		menu.addItem(new MenuItem("Show all users", operations, "showAllUsers"));
		menu.addItem(new MenuItem("Show databases (admin only)", operations, "showTables"));
		menu.execute();
	}
	
	/**
	 * Metodo di testing che esegue tutte le operazioni possibili presenti nel database 
	 * in automatico in modo tale da mostrare tutto ció che é possibile fare nell' ecommerce
	 * con un solo comando.
	 */
	public void showcase() {
		login.setCurrentUser(login.login("admin", "admin"));
		operations.testAdminOperations();
		login.logout();
	}
}
