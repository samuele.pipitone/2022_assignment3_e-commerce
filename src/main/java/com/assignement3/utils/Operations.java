package com.assignement3.utils;

import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.util.List;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.assignement3.Ecommerce.EcommerceApplication;
import com.assignement3.console.ConsoleUtils;
import com.assignement3.model.Carrello;
import com.assignement3.model.Film;
import com.assignement3.model.Libro;
import com.assignement3.model.Musica;
import com.assignement3.model.Oggetto;
import com.assignement3.model.Utente;
import com.assignement3.repository.CarrelloRepository;
import com.assignement3.repository.OggettoRepository;
import com.assignement3.repository.UtenteRepository;

public class Operations {
	
	private static Logger LOG = LoggerFactory.getLogger(EcommerceApplication.class);
	
	private OggettoRepository oggettoRepository;
	private CarrelloRepository carrelloRepository;
	private UtenteRepository utenteRepository;
	private DataSource dataSource;
	
	public Operations(OggettoRepository oggettoRepository, CarrelloRepository carrelloRepository, UtenteRepository utenteRepository, DataSource dataSource) {
		this.carrelloRepository = carrelloRepository;
		this.oggettoRepository = oggettoRepository;
		this.utenteRepository = utenteRepository;
		this.dataSource = dataSource;
	}
	
	/**
	 * Create some initial objects to populate database
	 */
	public void initDatabase(Utente admin) {
		utenteRepository.save(admin);
		
		Utente u1 = new Utente("Silvio", "admin");
		Utente u2 = new Utente("Marco", "admin");
		Utente u3 = new Utente("Anna", "admin");
		Utente u4 = new Utente("Giulia", "admin");
		utenteRepository.save(u1);
		utenteRepository.save(u2);
		utenteRepository.save(u3);
		utenteRepository.save(u4);
		
		Oggetto o1 = new Oggetto("Joypad", 45.99, "Simple joypad", admin);
		Oggetto o2 = new Oggetto("Scissor", 4.99, "Used to cut", u1);
		Oggetto o3 = new Oggetto("Lamp", 11, "Lamp from Ikea", u2);
		Oggetto o4 = new Oggetto("Phone", 300, "iPhone 5", u3);
		Oggetto o5 = new Oggetto("Chair", 40, "Chair from Ikea", u4);
		Libro l1 = new Libro("It", 15.95, "Horror book", 450, "Stephen King", admin);
		Libro l2 = new Libro("It", 15.95, "Horror book", 450, "Stephen King", u1);
		Musica m1 = new Musica("Let it be", 2, "song from the Beatles", 230, "The Beatles", u2);
		Film f1 = new Film("Avatar 2", 10, "second film of the saga", 192, u3);
		Film f2 = new Film("Shutter Island", 15, "film of Martin Scorsese", 132, u4);
		
		oggettoRepository.save(o1);
		oggettoRepository.save(o2);
		oggettoRepository.save(o3);
		oggettoRepository.save(o4);
		oggettoRepository.save(o5);
		oggettoRepository.save(l1);
		oggettoRepository.save(l2);
		oggettoRepository.save(m1);
		oggettoRepository.save(f1);
		oggettoRepository.save(f2);
	}
	
	/**
	 * Add items to a cart. If you don't have any carts you can first create one.
	 */
	public void addItemToCart() {
		List<Carrello> listCarrelli = carrelloRepository.findCarrelliByUser(Login.currentUser);
		if(listCarrelli.size() == 0 || listCarrelli == null) {
			System.out.println("You don't have any carts!");
			addNewCart();
		}
		listCarrelli = carrelloRepository.findCarrelliByUser(Login.currentUser);
		int i = 1;
		for(Carrello c : listCarrelli) {
			System.out.println(i + ") " + c.getName());
			i++;
		}
		
		int cartIndex = 
				ConsoleUtils.integerInput("Select the cart number:", 1, listCarrelli.size());
		
		List<Oggetto> oggetti = oggettoRepository.getAllItems();
		

		 for (int k = 0; k < oggetti.size(); k++) {
		     Oggetto o = oggetti.get(k);
		     if(o.getSeller() != null) {
		    	 if (o.getSeller().equals(Login.currentUser)) {
				    	oggetti.remove(o);
				        k--;
				     }
		     }
		  }
		
		int j = 1;
		for(Oggetto o : oggetti) {
			System.out.println(j + ") " + o.toString());
			j++;
		}
		
		int itemIndex = 
				ConsoleUtils.integerInput("Select the item number:", 1, oggetti.size());
		
		Oggetto oggetto = oggetti.get(itemIndex - 1);
		listCarrelli.get(cartIndex - 1).addObject(oggetto);
		carrelloRepository.save(listCarrelli.get(cartIndex - 1));
		System.out.println("Item saved");

	}
	
	/**
	 * Print all the items contained in the database.
	 */
	public void showAllItems() {
		oggettoRepository.showAllItems();
	}
	
	/**
	 * Let the user sell an item (book, music or film).
	 * Fields of the item are taken in input from by the user.
	 */
	public void sell() {

		String objName = "";
		String objDescription = "";
		double objPrice = 0;
		int nPages = 0;
		String bookAuthor = "";
		double musicLength = 0;
		String musicAuthor = "";
		double filmDuration = 0;

		System.out.println("1) Book");
		System.out.println("2) Music");
		System.out.println("3) Film");
		
		int userChoice = ConsoleUtils.integerInput("Select the item type:", 1, 3);
		
		switch(userChoice) {
			case 1: {
				objName = ConsoleUtils.stringInput("Insert the name of the object:", 5);
				objPrice = ConsoleUtils.doubleInput("Insert price:");
				objDescription = ConsoleUtils.stringInput("Insert a short description:", 5);
				nPages =  ConsoleUtils.integerInput("Insert number of pages:");
				bookAuthor = ConsoleUtils.stringInput("Insert the name of the author:");
				Libro libro = new Libro(objName, objPrice, objDescription, nPages, bookAuthor);
				try {
					libro.setSeller(Login.currentUser);
					oggettoRepository.save(libro);
					System.out.println("Success, object saved!");
				} catch(Exception e) {
					System.out.println("Error! object not saved");
				}
				break;
			}
			case 2: {
				objName = ConsoleUtils.stringInput("Insert the name of the object:", 5);
				objPrice = ConsoleUtils.doubleInput("Insert price:");
				objDescription = ConsoleUtils.stringInput("Insert a short description:", 5);
				musicLength = ConsoleUtils.doubleInput("Insert the song length:");
				musicAuthor = ConsoleUtils.stringInput("Insert the author of the song:");
				Musica musica = new Musica(objName, objPrice, objDescription, musicLength, musicAuthor);
				try {
					musica.setSeller(Login.currentUser);
					oggettoRepository.save(musica);
					System.out.println("Success, object saved!");
				} catch(Exception e) {
					System.out.println("Error! object not saved");
				}
				break;
			}
			case 3: {
				objName = ConsoleUtils.stringInput("Insert the name of the object:", 5);
				objPrice = ConsoleUtils.doubleInput("Insert price:");
				objDescription = ConsoleUtils.stringInput("Insert a short description:", 5);
				filmDuration = ConsoleUtils.doubleInput("Insert the duration:");
				Film film = new Film(objName, objPrice, objDescription, filmDuration);
				try {
					film.setSeller(Login.currentUser);
					oggettoRepository.save(film);
					System.out.println("Success, object saved!");
				} catch(Exception e) {
					System.out.println("Error! object not saved");
				}
				break;
			}
			default: break;
		}
	}
	
	/**
	 * Used to search any items by exact match of name.
	 */
	public void searchItem() {
		String name = ConsoleUtils.stringInput("Insert the name of the item:");
		try {
			oggettoRepository.findByName(name);
		} catch (Exception e) {
			System.out.println("No item found with that name!");
		}
	}
	
	/**
	 * Shows all the carts you have and then ask for the number of the cart
	 * you want to see.
	 */
	public void showCart() {
		
		List<Carrello> listCarrelli = carrelloRepository.findCarrelliByUser(Login.currentUser);
		int userInput = 0;
		
		if(listCarrelli != null && listCarrelli.size() > 0) {
			
			System.out.println("You have this cart/s:");
			int i = 1;
			for(Carrello carrello : listCarrelli) {
				System.out.println(i + ") " + carrello.getName());
				i++;
			}
			
			userInput = ConsoleUtils.integerInput("Choose a cart to see:", 1, listCarrelli.size());
			
					
			List<Oggetto> oggetti = carrelloRepository.findOggettiByCarrelloId(
					(long) listCarrelli.get(userInput - 1).getId());
			
			if(oggetti != null && oggetti.size() > 0) {
				System.out.println("You cart contain: ");
				
				for(Oggetto oggetto : oggetti) {
					System.out.println(oggetto.toString());
				}
				return;
			} else {
				System.out.println("Your cart is empty!");
				return;
			}
		} else {
			System.out.println("Sorry you don't have any cart to choose");
		}
	}
	
	/**
	 * Add a new cart to the current user
	 */
	public void addNewCart() {
		String cartName = ConsoleUtils.stringInput("Insert name of the new cart:");
		Carrello carrello = new Carrello(cartName);
		carrello.setUser(Login.currentUser);
		carrelloRepository.save(carrello);
	}
	
	/**
	 * Make the current user follow another user in the database.
	 * Also checks if you already follow that user and doesn't make you 
	 * follow him twice.
	 */
	public void followUser() {
		Utente user = Login.currentUser;
		List<Utente> otherUsers = utenteRepository.customFindOthers(user.getUsername());
		
		if(otherUsers != null && !otherUsers.isEmpty()) {
			int i = 1;
			for(Utente u : otherUsers) {
				System.out.println(i + ") " + u.getUsername());
				i++;
			}

			int userIndex = ConsoleUtils.integerInput("Choose a user to follow:", 1, otherUsers.size());
			
			try {
				user.addFollowing(otherUsers.get(userIndex - 1));
				System.out.println("User added to following list");
				utenteRepository.save(user);
				
			} catch (Exception e) {
				System.out.println("User already followed!");
			}
		}
		else {
			System.out.println("There are no other users!");
		}
	}
	
	/**
	 * Prints a list of all other users that follow the current user
	 */
	public void showFollowers() {
		List<Utente> followers = utenteRepository.customFindFollowers(Login.currentUser.getId());
		if(followers != null && !followers.isEmpty()) {
			System.out.println("List of users that follows you:");
			for(Utente u : followers) {
				System.out.println(u.toString());
			}
		}
		else {
			System.out.println("You have no followers yet!");
		}
	}
	
	/**
	 * Prints a list of all users
	 */
	public void showAllUsers() {
		List<Utente> users = utenteRepository.getAllUsers();
		for(Utente u : users) {
			System.out.println(u.toString());
		}
	}
	
	/**
	 * Make the current user buy an item directly from the store
	 * given its name in input.
	 * 
	 * @throws Exception
	 */
	public void buy() {
		
		System.out.println("This is the list of the items available:");
		oggettoRepository.show();
		String nameChoosen = ConsoleUtils.stringInput("Select the exact name of the item you want to buy:");
		Oggetto item = oggettoRepository.buyItem(nameChoosen);
		
		if(item != null) {

				
			if(utenteRepository.customFindFollowers(item.getSeller().getId()).contains(Login.currentUser)) {
				System.out.println("Congratulation! your purchase was successfull, the price is: " 
						+ item.getPrice() * .85 + "$ (the price was discounted by 15% because you follow the seller!)");
			}
			else {
				System.out.println("Congratulation! your purchase was successfull, the price is: " + item.getPrice());
			}
		} else {
			System.out.println("Sorry we could not find your item!");
		}
	}
	
	/**
	 * Show the tables created at runtime by the Hibernate environment
	 * 
	 * @throws Exception
	 */
	public void showTables() throws Exception {
		if(Login.currentUser.getUsername().equals("admin")) {
			DatabaseMetaData metaData = dataSource.getConnection().getMetaData();
			ResultSet tables = metaData.getTables(null, null, null, new String[] { "TABLE" });
			while (tables.next()) {
				String tableName = tables.getString("TABLE_NAME");
				LOG.info(tableName);
				ResultSet columns = metaData.getColumns(null, null, tableName, "%");
				while (columns.next()) {
					String columnName = columns.getString("COLUMN_NAME");
					LOG.info("\t" + columnName);
				}
			}
		}
		else {
			System.out.println("you have to be the admin to show the database structure!");
		}
	}
	
	/**
	 * Perform a test of all the possible repositories operations.
	 */
	public void testAdminOperations() {
		// l'admin vende un libro
		Libro libro1 = new Libro("The Great Gatsby", 20, "a book", 300, "F. Scott Fizgerald", Login.currentUser);
		oggettoRepository.save(libro1);
		System.out.println(Login.currentUser.getUsername() + " vende il libro: " + libro1 + "\n");
		
		// l'admin compra un libro di un altro utente (giá esistente nel database)
		Oggetto oggetto1 = oggettoRepository.buyItem("Scissor");
		System.out.println(Login.currentUser.getUsername() + " compra l'oggetto: " + oggetto1 + "\n");
		
		// l'admin cerca un oggetto in base al suo nome (serve gestire un eccezione nel caso l'oggetto non sia presente)
		Oggetto oggetto2 = null;
		try {
			oggetto2 = oggettoRepository.findByName("Shutter Island");
			System.out.println(Login.currentUser.getUsername() + " cerca l' oggetto: " + oggetto2 + "\n");
		} catch (Exception e) {
			System.out.println("L'oggetto non é stato trovato!");
		}
		
		// l'admin vuole vedere tutti gli oggetti presenti nell' e-commerce
		System.out.println(Login.currentUser.getUsername() + " richiede la lista di tutti gli oggetti: ");
		oggettoRepository.showAllItems();
		System.out.println();
		
		// l'admin crea un carrello chiamato carrello1
		Carrello carrello1 = new Carrello("carrello1");
		carrelloRepository.save(carrello1);
		System.out.println(Login.currentUser.getUsername() + " crea un carrello: " + carrello1 + "\n");
		
		// l'admin aggiunge un oggetto al carrello appena creato
		List<Carrello> carrelli = 
				carrelloRepository.findCarrelliByUser(Login.currentUser);
		for(Carrello c : carrelli)
			if(carrello1.equals(c)) {
				carrello1.addObject(oggetto2);
			}
		System.out.println(Login.currentUser.getUsername() + " aggiunge l'oggetto: "
				+ oggetto2 + " al carrello " + carrello1 + "\n");
		
		// l'admin segue un altro utente
		Utente silvio = utenteRepository.customFindByUsername("Silvio");
		try {
			Login.currentUser.addFollowing(silvio);
			utenteRepository.save(silvio);
			utenteRepository.save(Login.currentUser);
		} catch (Exception e) {
			System.out.println("L'utente non é stato trovato!");
		}
		System.out.println(Login.currentUser.getUsername() + " aggiunge l'utente: "
				+ silvio + " alla lista dei seguiti\n");
	}
}
