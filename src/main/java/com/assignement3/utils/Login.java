package com.assignement3.utils;

import java.util.Scanner;

import com.assignement3.model.Utente;
import com.assignement3.repository.UtenteRepository;

public class Login {
	
	//Keeps track of the currently logged user
	static public Utente currentUser;
	
	private UtenteRepository utenteRepository;
	
	public Login(UtenteRepository utenteRepository) {
		this.utenteRepository = utenteRepository;
	}
	
	public Utente createRuntimeUser() {
		//add an admin user at runtime
		return new Utente("admin", "admin");
	}
	
	public void authenticate() {
		
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(System.in);
		
		//login loop
		System.out.println("Please insert your username");
		String username = scanner.nextLine();
		System.out.println("Please insert your password");
		String password = scanner.nextLine();
		
		Utente user = login(username, password);
		
		while(user == null) {
			System.out.println("Username/Password incorrect! (admin/admin)");
			
			System.out.println("Please insert your username");
			username = scanner.nextLine();
			System.out.println("Please insert your password");
			password = scanner.nextLine();
			
			user = login(username, password);
		}
		
		currentUser = user;
		
		System.out.println("Welcome " + username + "!");
		//end login loop
	}
	
	public Utente login(String username, String password) {
		return utenteRepository.customFindByUsernamePassword(username, password);
	}
	
	public void register() {
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(System.in);
		
		//register loop
		System.out.println("Please insert your username");
		String username = scanner.nextLine();
		
		Utente user = utenteRepository.customFindByUsername(username);
		
		while(user != null) {
			System.out.println("Username already in use!");
			System.out.println("Please insert your username");
			username = scanner.nextLine();
			
			user = utenteRepository.customFindByUsername(username);
		}
		
		System.out.println("Please insert your password");
		String password = scanner.nextLine();
		
		user = new Utente(username, password);
		utenteRepository.save(user);
		currentUser = user;
		
		System.out.println("Welcome " + username + "!");
		//end login loop
		
	}
	
	public void setCurrentUser(Utente user) {
		currentUser = user;
	}
	
	public void logout() {
		currentUser = null;
	}
	
}
