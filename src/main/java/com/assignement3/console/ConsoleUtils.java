package com.assignement3.console;

import java.util.Scanner;
 
public class ConsoleUtils {
	private static Scanner console = new Scanner(System.in);
	
	/**
	 * Perform a simple pause of the input, requiring the user to click Enter to continue
	 */
	public static void pauseExecution() {
		System.out.print("Press Enter to Continue... ");
		console.nextLine();
	}
	
	/**
	 * Perform a user confirmation for a possible action that needs it
	 * @return true if the user confirmed, false otherwise
	 */
	public static boolean requestConfirmation() {
		while (true) {
			System.out.print("Confirm Operation (y/n)... ");
			String in = console.nextLine().toLowerCase();
			if (in.equals("y") || in.equals("yes"))
				return true;
			else if (in.equals("n") || in.equals("no"))
				return false;
		}
	}
	
	/**
	 * Perform a user input of type integer within a range of numbers {minIndex; maxIndex}
	 * @label prints this instruction before taking the input
	 * @param minIndex
	 * @param maxIndex
	 * 
	 * @return the valid number inserted by the user
	 */
	public static int integerInput(String label, int minIndex, int maxIndex) {
		System.out.println(label);
		int number = -1;
		boolean firstExec = true;
		do {
			if(!firstExec) {
				System.out.println("That's not a valid integer number!");
				System.out.println(label);
			}
		    while (!console.hasNextInt()) {
		        System.out.println("That's not an integer number!");
		        System.out.println(label);
		        console.next();
		    }
		    number = console.nextInt();
		    firstExec = false;
		} while (number < minIndex || number > maxIndex);
		return number;
	}
	
	/**
	 * Perform a user input of type integer and checks if it's positive
	 * @label prints this instruction before taking the input
	 * 
	 * @return the valid number inserted by the user
	 */
	public static int integerInput(String label) {
		System.out.println(label);
		int number = -1;
		boolean firstExec = true;
		do {
			if(!firstExec) {
				System.out.println("That's not a positive integer number!");
				System.out.println(label);
			}
		    while (!console.hasNextInt()) {
		        System.out.println("That's not an integer number!");
		        System.out.println(label);
		        console.next();
		    }
		    number = console.nextInt();
		    firstExec = false;
		} while (number <= 0);
		return number;
	}
	
	/**
	 * Perform a user input of type double and checks if it's positive
	 * @label prints this instruction before taking the input
	 * 
	 * @return the valid double inserted by the user
	 */
	public static double doubleInput(String label) {
		System.out.println(label);
		int number = -1;
		boolean firstExec = true;
		do {
			if(!firstExec) {
				System.out.println("That's not a positive double number!");
				System.out.println(label);
			}
		    while (!console.hasNextDouble()) {
		    	while(!console.hasNextInt()) {
		    		System.out.println("That's not a double number!");
		    		System.out.println(label);
			        console.next();
		    	}
		    }
		    number = console.nextInt();
		    firstExec = false;
		} while (number <= 0);
		return number;
	}
	
	/**
	 * Perform a user input of type string with a minimum length of minLength
	 * @label prints this instruction before taking the input
	 * @param minLength
	 * 
	 * @return the valid string inserted by the user
	 */
	public static String stringInput(String label, int minLength) {
		System.out.println(label);
		String word = "";
		boolean firstExec = true;
	    do {
	    	if(!firstExec) {
				System.out.println("The input should be at least " + minLength + " characters!");
				System.out.println(label);
			}
	        word = console.next();
	        firstExec = false;
	    } while(word.length() < minLength);
		return word;
	}
	
	/**
	 * Perform a user input of type string with no restriction in the length of the string
	 * @label prints this instruction before taking the input
	 * 
	 * @return the valid string inserted by the user
	 */
	public static String stringInput(String label) {
		System.out.println(label);
		return console.next();
	}
}